# python server image
FROM python:stretch

# Directory Setup
COPY . /app
WORKDIR /app

# Dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Server Config.
ENTRYPOINT ["gunicorn", "-b", ":8080", "main:APP"]
